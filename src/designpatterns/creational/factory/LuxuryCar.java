package designpatterns.creational.factory;

public class LuxuryCar extends Car {
	LuxuryCar() {
		super(CarType.LUXURY);
		construct();
	}

	protected void construct() {
		System.out.println("Building luxury car");

	}

}

