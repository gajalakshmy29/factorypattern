package designpatterns.creational.factory;

public abstract class Car {
  private CarType model = null;
public Car (CarType model) {
	this.model = model;
	arrangeparts();
}
private void arrangeparts() {
	
}
public CarType getmodel() {
	return model;
}
public void setmodel(CarType model) {
	this.model = model;
}
}
