package designpatterns.creational.factory;

public class CarFactory {
	public static Car buildCar(CarType model) {
		Car Car = null;
		switch (model) {
		case SMALL:
			Car = new SmallCar();
			break;

		case SEDAM:
			Car = new SedamCar();
			break;

		case LUXURY:
			Car = new LuxuryCar();
			break;

		default:
			//throw some exception
			
			break;

		}
		return Car;
	}

}
