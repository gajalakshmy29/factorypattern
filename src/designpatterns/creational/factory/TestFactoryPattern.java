package designpatterns.creational.factory;

public class TestFactoryPattern {
public static void main(String args []) {
	System.out.println(CarFactory.buildCar(CarType.SMALL));
	System.out.println(CarFactory.buildCar(CarType.SEDAM));
	System.out.println(CarFactory.buildCar(CarType.LUXURY));
	
}
}
