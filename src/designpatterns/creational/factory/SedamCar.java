package designpatterns.creational.factory;

public class SedamCar extends Car {
	SedamCar() {
		super(CarType.SEDAM);
		construct();
	}

	protected void construct() {
		System.out.println("Building sedan car");
	   
	      }


	

}
